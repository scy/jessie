#!/usr/bin/env python3

import csv
from datetime import datetime, timedelta
import logging
import re
import sys
from typing import List, Optional

from pydantic import BaseModel, ConstrainedStr, NonNegativeFloat, PositiveFloat, PositiveInt, validator


_logger = logging.getLogger(__name__)


class Currency(ConstrainedStr):
    regex = re.compile(r"^[A-Z]{3}$")


class FuelEntry(BaseModel):
    time: datetime
    liters: Optional[PositiveFloat]
    currency: Optional[Currency]
    price: Optional[PositiveFloat]
    odometer: Optional[PositiveInt]
    trip: Optional[NonNegativeFloat]
    avg_cons: Optional[PositiveFloat]
    avg_speed: Optional[PositiveInt]
    duration: Optional[timedelta]
    location: str
    comment: Optional[str]

    @property
    def price_per_liter(self) -> float:
        return self.price / self.liters if self.price and self.liters else None

    def check_against_prev(self, prev: "FuelEntry") -> List[str]:
        warn = []

        if self.time < prev.time:
            warn.append("younger than previous entry")

        if self.liters and self.price and self.currency == "EUR":
            if self.price_per_liter == 1 or not 0.95 < self.price_per_liter < 3:
                warn.append(f"suspicious price: {self.price_per_liter:.3f} {self.currency:.3}/l")

        if self.odometer and prev.odometer:
            if self.odometer < prev.odometer:
                warn.append("odometer moved backwards")
            if self.trip:
                odo_delta = abs((prev.odometer + self.trip) - self.odometer)
                if odo_delta > 1.5:
                    warn.append(f"trip/odometer discrepancy: {prev.odometer} + {self.trip} != {self.odometer} (delta={odo_delta:.1f})")

        return warn


def reader(file):
    reader = csv.DictReader(file, restkey="")
    for line in reader:
        yield {
            k.strip().replace("date/", ""): v
            for k, v in line.items()
        }


def parser(items):
    lengths: Optional[Dict[str, int]] = None
    for item in items:
        id = item["time"]
        if "" in item:
            _logger.warning(f"{id} has wrong number of fields")
            del item[""]
        if lengths:
            wrong_length = [
                k
                for k, v in item.items()
                if k in lengths and len(v) != lengths[k]
            ]
            if wrong_length:
                _logger.warning(f"{id} has field(s) with wrong length: {', '.join(wrong_length)}")
        else:
            lengths = {
                k: len(v)
                for k, v in item.items()
                if k not in ("location", "comment")  # var length fields
            }
        item["currency"], item["price"] = item["price"].split(" ", 1)
        item = {k: v.strip() for k, v in item.items()}
        entry = FuelEntry.parse_obj({
            k: None if v == "" else v
            for k, v in item.items()
        })
        if item["duration"]:
            item["duration"] = f"{item['duration']}:00"
        yield entry


def lint(file):
    prev: Optional[FuelEntry] = None
    for entry in parser(reader(file)):
        if prev:
            for warn in entry.check_against_prev(prev):
                _logger.warning(f"{str(entry.time)[:-3]} {warn}")
        prev = entry


if __name__ == "__main__":
    lint(sys.stdin)
