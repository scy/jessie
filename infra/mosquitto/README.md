# Mosquitto config

The Raspberry Pi in Jessie acts as the local MQTT hub.

Check out [`jessie.conf`](jessie.conf) for its config.
This is supposed to be placed into `/etc/mosquitto/conf.d`.

There's also the [`acl`](acl) file that goes into `/etc/mosquitto`.
