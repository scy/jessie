# Code in Jessie

You can find the following projects in the subdirectories:

* [`artoo`](artoo): Microcontroller with on-board display to show helpful information while I'm driving.
* [`e3372`](e3372): Publish connection quality of my LTE uplink to MQTT.
* [`glance`](glance): Vuetify web frontend that talks to the components using MQTT.
* [`gnss`](gnss): Publish GNSS position to MQTT (and retrieve it from there). Additionally, fetch weather reports.
* [`homn`](homn): Read buttons in the interior and toggle relays based on that.
* [`phlox`](phlox): Microcontroller to monitor power and water levels, solar input etc.
* [`tasker`](tasker): Tasker plugin to display uplink quality.

## Environment variables

Some of that code is configured using environment variables.
And since some of these variables are required by more than one of these tools, it makes sense to document them in this top-level directory.

* `JESSIE_GNSS_DEVICE`: Linux device path to the GNSS receiver, e.g. `/dev/ttyACM0`.
* `JESSIE_GNSS_DISTANCE_MIN`: Don't publish position updates if we didn't at least move `x` meters (default `2`).
* `JESSIE_GNSS_INTERVAL_MIN`: Don't publish position updates faster than every `x` seconds (default `0.2`).
* `JESSIE_GNSS_INTERVAL_MAX`: Publish a position update at least roughly every `x` seconds (default `30`), even if we did not move.
* `JESSIE_MQTT_HOST`: Hostname of the MQTT server.
* `JESSIE_MQTT_PASS`: Password for logging in to the MQTT server.
* `JESSIE_MQTT_PREFIX`: MQTT topic prefix for everything sent to and received from MQTT, e.g. `jessie/`.
* `JESSIE_MQTT_USER`: Username for logging in to the MQTT server.

## Installation

Each subproject probably has its own installation instructions, but some may come with systemd service definitions.

To install them, first make sure that you have set the environment variables.
To do that, write them as key-value pairs (delimited by `=`, one per line) to something like `~/.config/environment.d/jessie.conf`.
See the `environment.d` man page for details.
You probably also want to set `PYTHONUNBUFFERED=1` there, to be able to inspect the `journalctl` logs of Python services more easily.
Make sure to reload your systemd user daemon afterwards (`systemctl --user daemon-reload`) and check whether the variables have been applied (`systemctl --user show-environment`).

The service files need to be copied (or symlinked?) to `~/.config/systemd/user`.
You can then start them using `systemctl --user enable --now whatever.service`.

If you'd like systemd to run your services regardless of whether or not you're logged in (I assume you'd want that), run `sudo loginctl enable-linger YOUR-USERNAME`.

## MQTT topics

These services communicate over MQTT.
All of the topics start with what's configured at `JESSIE_MQTT_PREFIX`, which should be `jessie/`.
The following topics exist:

* `cellular/connection`: JSON-encoded cellular uplink connection status and quality (see [`e3372`](e3372).
* `cellular/usage`: JSON-encoded usage statistics and projection for the LTE uplink, provided by [vodafone-quickcheck-scrape](https://codeberg.org/scy/vodafone-quickcheck-scrape).
* `gnss/packed`: Compact binary position, heading and speed data, with multiple updates per second if moving fast (see [`gnss`](gnss)).
* `weather/report`: A somewhat detailed weather report for Jessie's current location in JSON format, powered by [wttr.in](https://wttr.in/) and provided by [`gnss`](gnss). It will update every 10 minutes when I'm moving, else every 2 hours.
