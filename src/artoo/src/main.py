from machine import I2C, Pin
import json
import struct
import time

from perthensis import MQTTClient, Scheduler, WLANClient
from sh1106 import SH1106_I2C


TOPIC_CELL_CONN = b"jessie/cellular/connection"
TOPIC_GNSS_PACK = b"jessie/gnss/packed"


def bits(b, start, count):
    result = 0
    for byte in b:
        if count <= 0 or start < 0:
            return result
        if start < 8:
            masklen = count if count <= 8 else count % 8
            maskshift = 8 - (start + masklen)
            if result != 0:
                result = result << masklen
            mask = ((1 << masklen) - 1)
            result |= (byte >> maskshift) & mask
            count -= masklen
            start = 0
        else:
            start -= 8
    return result


def ieee754(b, mant_len):
    exp_len = 8 * len(b) - mant_len - 1
    sign = -1 if b[0] & 0b10000000 else 1
    exp = bits(b, 1, exp_len)
    exp_val = exp - 15
    mant = bits(b, exp_len + 1, mant_len)
    result = sign * ((2 ** exp_val) * ((0 if exp == 0 else 1) + (mant / 1024)))
    return result


def on_mqtt(topic, msg):
    global display
    print(topic, msg)
    if topic == TOPIC_CELL_CONN:
        d = json.loads(msg)
        display.fill_rect(0, 0, 64, 8, 0)
        display.text("{0} {1}".format(d["bars"], d["type"]), 0, 0, 1)
        display.show()
    elif topic == TOPIC_GNSS_PACK:
        (ts, lat, lon, alt, spd, crs, stat) = struct.unpack("!dff2s2s2sB", msg)
        (alt, spd, crs) = (ieee754(b, 10) for b in (alt, spd, crs))
        display.fill_rect(0, 8, 64, 8, 0)
        display.text("{0:3d} km/h".format(round(spd)), 0, 8, 1)
        display.show()
        print(ts, lat, lon, alt, spd, crs, stat)


display_i2c = I2C(1, scl=Pin(5, Pin.OUT), sda=Pin(4, Pin.OUT))
display = SH1106_I2C(128, 64, display_i2c, None, rotate=90)
display.init_display()

with open("config.json", "r") as f:
    config = json.load(f)

sch = Scheduler()

wlan = WLANClient(config["ssid"], config["psk"])
sch(wlan.watch)
wlan.enable()

mqtt = MQTTClient(
    config["mqtt_host"],
    user=config["mqtt_user"], password=config["mqtt_pass"],
    client_id="artoo",
)
mqtt.set_callback(on_mqtt)
mqtt.subscribe(TOPIC_CELL_CONN)
mqtt.subscribe(TOPIC_GNSS_PACK)
sch(mqtt.watch)

sch.run_forever()
