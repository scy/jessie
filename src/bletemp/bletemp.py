import collections
from functools import partial
from os import environ
from struct import unpack
from threading import Thread
from time import sleep, time

from bluepy.btle import DefaultDelegate, Scanner
import requests


NAMETRANS = {
    "4c65a8dc0678": "cockpit",
    "4c65a8dc2907": "food_storage",
    "4c65a8dc1b6a": "desk",
    "4c65a8dc1c2c": "bathroom",
    "4c65a8dc30a6": "bed",
    "582d343b2421": "below_kitchen",
    "582d343b2429": "over_cockpit",
    "582d343b2553": "heating_valve",
    "582d343b279c": "water_tank",
    "582d343b288e": "fridge",
}


def parse_svc_data(data):
    (uuid,) = unpack("<H", data[0:2])
    if uuid == 0xfe95:
        (t,) = unpack("<H", data[13:15])
        vlen = data[15]
        val = data[16:16+vlen]
        if t == 0x1004 and vlen == 2:  # temperature
            (temp,) = unpack("<h", val)
            return {"temp_c": temp/10}
        if t == 0x1006 and vlen == 2:  # humidity
            (hum,) = unpack("<H", val)
            return {"hum_pct": hum/10}
        if t == 0x100a and vlen == 1:  # battery
            (bat,) = unpack("<B", val)
            return {"bat_pct": bat}
        if t == 0x100d and vlen == 4:  # temperature + humidity
            temp, hum = unpack("<hH", val)
            return {"temp_c": temp/10, "hum_pct": hum/10}


class ScanDelegate(DefaultDelegate):
    def __init__(self, deque):
        self._deque = deque
        super().__init__()

    def handleDiscovery(self, dev, isNewDev, isNewData):
        for adtype, _, val in dev.getScanData():
            if adtype == 0x16:
                now = round(time())
                vals = None
                try:
                    vals = parse_svc_data(bytes.fromhex(val))
                except Exception:
                    pass
                if not vals:
                    continue
                name = dev.addr.replace(":", "")
                name = NAMETRANS.get(name, name)
                self._deque.extend({
                    "name": f"jessie.temphum.{name}.{k}",
                    "value": v,
                    "time": now,
                    "interval": 60,
                } for k, v in vals.items())


def submit_entries(deque):
    while True:
        sleep(10)
        to_send = list(deque)
        if to_send:
            try:
                res = requests.post(
                    environ["GRAPHITE_URL"],
                    headers={
                        "Authorization": f"Bearer {environ['GRAPHITE_APIKEY']}",
                    },
                    json=to_send,
                )
                if res.status_code < 500:
                    # Item(s) are either faulty or were handled successfully.
                    for item in to_send:
                        try:
                            deque.remove(item)
                        except ValueError:
                            # No longer in the deque, maybe got pushed off.
                            pass
                print(res, res.content)
            except Exception:
                # Retry.
                pass


def main():
    deque = collections.deque((), 1000)
    Thread(target=partial(submit_entries, deque), name="Submitter", daemon=True).start()
    scanner = Scanner().withDelegate(ScanDelegate(deque))
    while True:
        scanner.scan(0.25)
        sleep(0.5)


if __name__ == "__main__":
    main()
