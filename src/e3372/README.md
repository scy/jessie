# Huawei E3372 connection status to MQTT

## Output format

Every interval (5 seconds), a JSON object is published to MQTT.
It contains the following fields:

* `conn`: Uplink connection is established (`true` or `false`).
* `bars`: Signal strength in bars (`0` to the value of `of`).
* `g`: Generation of the network, as in "3G" or "4G". Can have decimal places. Example values: `0`, `2.75`, `4`.
* `gq`: Quality combined with generation. This is `(g * of) + bars`.
* `of`: How many bars can there be at maximum (usually `5`).
* `type`: Short string name of the connection technology, e.g. `"LTE (4G)"`.
