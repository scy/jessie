import json
from os import environ as env
from time import sleep
import xml.etree.ElementTree as ET

import paho.mqtt.client as mqtt
import requests


class StatusReader:

    network_type = {
        0  : (0   , "No Service"),
        1  : (1   , "GSM"),
        2  : (2.5 , "GPRS (2.5G)"),
        3  : (2.75, "EDGE (2.75G)"),
        4  : (3   , "WCDMA (3G)"),
        5  : (3.5 , "HSDPA (3G)"),
        6  : (3.5 , "HSUPA (3G)"),
        7  : (3.5 , "HSPA (3G)"),
        8  : (3   , "TD-SCDMA (3G)"),
        9  : (4   , "HSPA+ (4G)"),
        10 : (3.5 , "EV-DO rev. 0"),
        11 : (3.5 , "EV-DO rev. A"),
        12 : (3.5 , "EV-DO rev. B"),
        13 : (3   , "1xRTT"),
        14 : (3   , "UMB"),
        15 : (3   , "1xEVDV"),
        16 : (3   , "3xRTT"),
        17 : (3.5 , "HSPA+ 64QAM"),
        18 : (3.5 , "HSPA+ MIMO"),
        19 : (4   , "LTE (4G)"),
        41 : (3   , "UMTS (3G)"),
        44 : (3   , "HSPA (3G)"),
        45 : (3   , "HSPA+ (3G)"),
        46 : (3   , "DC-HSPA+ (3G)"),
        64 : (3   , "HSPA (3G)"),
        65 : (3   , "HSPA+ (3G)"),
        101: (4   , "LTE (4G)"),
    }

    def __init__(self, host="192.168.8.1"):
        self.host = host
        self.token = None

    def get_token(self):
        r = requests.get(f"http://{self.host}/api/webserver/SesTokInfo")
        root = ET.fromstring(r.content)
        if root.tag != "response":
            raise ValueError("no <response> element")
        sesinfo = root.find("SesInfo")
        if sesinfo is None:
            raise ValueError("no <SesInfo> element")
        k, v = str(sesinfo.text).split("=", 2)
        if k != "SessionID":
            raise ValueError("no SessionID= found")
        self.token = v
        return v

    def get_raw_status(self, retries=1):
        if self.token is None:
            self.get_token()
        r = requests.get(f"http://{self.host}/api/monitoring/status",
                         cookies={"SessionID": self.token})
        root = ET.fromstring(r.content)
        if root.tag != "response":
            if retries <= 0:
                raise ValueError(f"retrieving status failed, content {r.content}")
            self.token = None
            return self.get_raw_status(retries=retries-1)
        return dict({
            child.tag: None if child.text is None else str(child.text)
            for child in root
        })

    def get_nice_status(self):
        raw = self.get_raw_status()
        bars = int(raw.get("SignalIcon", 0))
        max_bars = int(raw.get("maxsignal", 5))
        cnt = raw.get("CurrentNetworkType", 0)
        g, name = self.network_type.get(
            int(raw.get("CurrentNetworkType", 0)),
            (0, f"#{cnt}"))
        return {
            "conn": raw.get("ConnectionStatus", None) == "901",
            "bars": bars,
            "of": max_bars,
            "g": g,
            "gq": round((g * max_bars) + bars),
            "type": name,
        }


if __name__ == "__main__":
    sr = StatusReader()
    client = mqtt.Client()
    client.username_pw_set(
        env.get("JESSIE_MQTT_USER", None),
        env.get("JESSIE_MQTT_PASS", None),
    )
    client.max_queued_messages_set(1)
    client.loop_start()
    client.connect_async(env.get("JESSIE_MQTT_HOST"))

    while True:
        try:
            status = json.dumps(
                sr.get_nice_status(),
                indent=None, separators=(",", ":"),
            )
            # print(status)
            client.publish(
                env.get("JESSIE_MQTT_PREFIX") + "cellular/connection",
                status,
            )
        except Exception as e:
            pass
        sleep(5)
