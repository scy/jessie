# Glance

This is the Vuetify web frontend for Jessie.

## Getting started

Use `npm install` to install the dependencies.

Define an environment variable `VUE_APP_MQTT_URL` that describes how to connect to and authenticate with the MQTT server, e.g. `wss://AzureDiamond:hunter2@example.com/mqtt`.

You can then run `npm run serve` for the development server with live reloads or `npm run build` for a production-optimized build.
