const EventEmitter = require("events");
const MQTT = require("async-mqtt");
const ieee754 = require("ieee754");

export const DISCONNECTED = 0;
export const CONNECTING = 1;
export const CONNECTED = 2;

export class Client extends EventEmitter {
	constructor() {
		super();
		this._state = DISCONNECTED;
	}

	get state() {
		return this._state;
	}

	connect(url) {
		this._setState(CONNECTING);

		this.mqtt = MQTT.connect(url);
		this.mqtt.on("error", (ev) => this._error(ev));
		this.mqtt.on("connect", (ev) => this._connected(ev));
		this.mqtt.on("message", (topic, payload, packet) => this._message(topic, payload, packet));

		this.mqtt.subscribe("jessie/cellular/connection");
		this.mqtt.subscribe("jessie/cellular/usage");
		this.mqtt.subscribe("jessie/gnss/packed");
		this.mqtt.subscribe("jessie/weather/report");
	}

	end() {
		return this.mqtt.end();
	}

	_connected() {
		this._setState(CONNECTED);
	}

	_error(err) {
		this._setState(CONNECTING, err);
	}

	_message(topic, payload) {
		if (topic === "jessie/gnss/packed") {
			this._msg_gnss_packed(payload);
		} else if (topic === "jessie/cellular/connection") {
			this.emit("cellularConnection", JSON.parse(payload.toString()));
		} else if (topic === "jessie/cellular/usage") {
			this.emit("cellularUsage", JSON.parse(payload.toString()));
		} else if (topic === "jessie/weather/report") {
			this.emit("weatherReport", JSON.parse(payload.toString()));
		}
	}

	_msg_gnss_packed(payload) {
		const quality = (payload[22] & 0xf0) >> 4;
		const sats = payload[22] & 0x0f;
		const decoded = {
			time: new Date(this._decodeFloat(8, payload, 0) * 1000),
			lat: this._decodeFloat(4, payload, 8),
			lon: this._decodeFloat(4, payload, 12),
			alt: this._decodeFloat(2, payload, 16),
			speed: this._decodeFloat(2, payload, 18),
			course: this._decodeFloat(2, payload, 20),
			qual: quality,
			sats,
		};
		this.emit("gnssDetailed", decoded);
	}

	_decodeFloat(bytes, payload, offset) {
		return ieee754.read(
			payload, offset, false,
			bytes === 8 ? 52 : (bytes === 4 ? 23 : 10),
			bytes);
	}

	_setState(state, err) {
		const oldState = this._state;
		this._state = state;
		this.emit("statechange", {
			oldState: oldState,
			newState: state,
			error: err ? err : null,
		});
	}

}
