# Jessie GNSS Tools

## Installation

Install the required Python packages:

```sh
pip3 install -r requirements.txt  # or whatever method you prefer
```

## `gnss2mqtt.py`

This will read position information from MQTT and publish it to MQTT in a somewhat smart way:

* When a position update arrives from the GNSS receiver (which can happen multiple times per second), don't publish it until at least 0.2 seconds have passed since the last publish. (Can be configured using `JESSIE_GNSS_INTERVAL_MIN`.)
* Also, if we did not move at least 2 meters (configured by `JESSIE_GNSS_DISTANCE_MIN`), don't publish either.
* However, if the last publish is at least 30 seconds (`JESSIE_GNSS_INTERVAL_MAX`) ago, _do_ publish, even if we did not move far enough. This can be used to check whether the service is still alive.

The values are published to the MQTT server (configured via [the respective variables](../README.md#environment-variables)), under the `<JESSIE_MQTT_PREFIX>/gnss/packed` topic.
The server has to support MQTTv5 and at least one topic alias.
We'll be using this feature to reduce the amount of bandwidth used.

Each message contains a composite binary value, 23 bytes per message.
All values are big endian.
They can be represented by the Python `struct` format string `!dffeeeB`.

* 8 byte `double`: Unix timestamp of the measurement, with fractional seconds
* 4 byte `float`: latitude
* 4 byte `float`: longitude
* 2 byte `half-float`: altitude in meters (`0` if unknown)
* 2 byte `half-float`: speed over ground in km/h
* 2 byte `half-float`: "true course" in degrees
* 1 byte `unsigned int`:
  * 4 MSB: fix quality:
    * `0`: invalid
    * `1`: GPS fix
    * `2`: DGPS fix
    * `6`: estimated
    * `15`: unknown (value has not been reported yet)
  * 4 LSB: number of visible satellites (`0` to `12`, `15` if unknown)

The service also publishes weather reports it's fetching from [wttr.in](https://wttr.in/) every 10 minutes (when I'm on the road) or every 2 hours (when I'm not), of course always for my current location.

## `mqttgnsslog.py`

This little script will subscribe to the GNSS messages coming in over MQTT, write them to disk (into the file `./jessie-gnss-log.dat`) and print them in a human-readable way.
