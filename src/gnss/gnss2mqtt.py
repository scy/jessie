import binascii
from datetime import datetime, timezone
import io
import json
from os import environ as env
import struct

from geopy.distance import geodesic
import paho.mqtt.client as mqtt
from paho.mqtt.properties import PacketTypes
import pynmea2
import requests
import serial

from weathermodel import Report


class WaitFor:
    def __init__(self, callback, min_interval, max_interval, min_distance, watch_speed=False):
        self.last_trigger = None
        self.last_pos = None
        self.last_speed = None
        self.callback = callback
        self.min_interval = min_interval
        self.max_interval = max_interval
        self.min_distance = min_distance
        self.watch_speed = watch_speed

    def update(self, ts, lat, lon, speed, params):
        # The minimum interval between triggers has not been reached yet,
        # do nothing.
        if self.last_trigger is not None and ts - self.last_trigger < self.min_interval:
            return

        # If we didn't move at least min_distance, the speed has not changed
        # (in case we're watching for that) and max_interval (i.e. the maximum
        # time between readings, even when idle) has not passed yet, do
        # nothing.
        if self.last_pos is not None and geodesic(self.last_pos, (lat, lon)).meters < self.min_distance:
            if not self.watch_speed or self.last_speed == speed:
                if ts - self.last_trigger < self.max_interval:
                    return

        self.last_trigger = ts
        self.last_pos = (lat, lon)
        self.last_speed = speed

        try:
            self.callback(**params)
        except Exception:
            pass


class Position:
    def __init__(self, sendfunc=None, weatherfunc=None,
                 min_pos_int=0.2, max_pos_int=30, min_pos_dist=2,
                 min_weather_int=600, max_weather_int=7200, min_weather_dist=10_000):
        self.loc_wait = WaitFor(self.update_loc, min_pos_int, max_pos_int, min_pos_dist, True)
        self.weather_wait = WaitFor(self.update_weather, min_weather_int, max_weather_int, min_weather_dist)
        self.last_qual = 15
        self.last_sats = 15
        self.last_dil = None
        self.last_alt = None
        self.sendfunc = sendfunc
        self.weatherfunc = weatherfunc

    def update(self, msg):
        if type(msg) == pynmea2.GGA:
            # Keep track of the values we're interested in.
            self.last_qual = msg.gps_qual
            self.last_sats = int(msg.num_sats)
            self.last_alt = msg.altitude
            return
        if type(msg) != pynmea2.RMC:
            return

        # Create a datetime & Unix timestamp from the GNSS time data.
        dt = datetime.combine(msg.datestamp, msg.timestamp, timezone.utc)
        ts = dt.timestamp()

        # Feed the WaitFor, which will call update_loc when it's time.
        (lat, lon) = (msg.latitude, msg.longitude)
        self.loc_wait.update(ts, lat, lon, msg.spd_over_grnd, {
            "dt": dt,
            "ts": ts,
            "lat": lat,
            "lon": lon,
            "speed": msg.spd_over_grnd,
            "course": msg.true_course,
        })
        self.weather_wait.update(ts, lat, lon, msg.spd_over_grnd, {"lat": lat, "lon": lon})

    def update_loc(self, dt, ts, lat, lon, speed, course):
        print(dt, (lat, lon), self.last_alt, msg.spd_over_grnd, msg.true_course, self.last_qual, self.last_sats)

        # If we have a function that is supposed to get packed data, call it.
        if self.sendfunc is None:
            return

        packed = struct.pack("!dffeeeB",
            ts,
            lat,
            lon,
            0 if self.last_alt is None else self.last_alt,
            speed * 1.852,
            course,
            (self.last_qual << 4) | self.last_sats,
        )
        self.sendfunc(packed)

    def update_weather(self, lat, lon):
        # If we have a function to send weather data, get a report.
        if self.weatherfunc is None:
            return

        r = requests.get(
            f"https://wttr.in/{round(lat, 3)},{round(lon, 3)}?format=j1&lang=de&m",
            headers={"Accept-Language": "de"},
        )
        m = Report.parse_obj(r.json())
        self.weatherfunc(m)


class MQTTSender:
    def __init__(self, topic, host, port=1883, user=None, passwd=None, weather_topic=None):
        self.topic = topic
        self.weather_topic = weather_topic
        self.alias_declared = False
        self.props = mqtt.Properties(PacketTypes.PUBLISH)
        self.props.TopicAlias = 1

        self.mqtt = mqtt.Client(protocol=mqtt.MQTTv5)
        if user is not None:
            self.mqtt.username_pw_set(user, passwd)
        self.mqtt.on_connect = self._on_connect
        self.mqtt.max_queued_messages_set(20)
        self.mqtt.loop_start()
        self.mqtt.connect_async(host, port)

    def _on_connect(self, client, userdata, flags, rc, props=None):
        self.alias_declared = False

    def _on_log(self, client, userdata, level, buf):
        print(buf)

    def send(self, packed):
        self.mqtt.publish(
            "" if self.alias_declared else self.topic,
            packed,
            qos=1,
            retain=True,
            properties=self.props,
        )
        self.alias_declared = True

    def send_weather(self, model):
        if self.weather_topic is None:
            return
        self.mqtt.publish(
            self.weather_topic,
            #json.dumps(model.dict(), indent=None, separators=(",", ":")),
            model.json(),
            qos=1,
            retain=True,
        )


ser = serial.Serial(env.get('JESSIE_GNSS_DEVICE'), timeout=0.1)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser, 1))
m = MQTTSender(
    env.get("JESSIE_MQTT_PREFIX") + "gnss/packed",
    env.get("JESSIE_MQTT_HOST"),
    user=env.get("JESSIE_MQTT_USER", None),
    passwd=env.get("JESSIE_MQTT_PASS", None),
    weather_topic=env.get("JESSIE_MQTT_PREFIX") + "weather/report",
)
p = Position(
    sendfunc=m.send, weatherfunc=m.send_weather,
    min_pos_int=float(env.get("JESSIE_GNSS_INTERVAL_MIN", 0.2)),
    max_pos_int=float(env.get("JESSIE_GNSS_INTERVAL_MAX", 30)),
    min_pos_dist=int(env.get("JESSIE_GNSS_DISTANCE_MIN", 2)),
)

while True:
    try:
        line = sio.readline()
        if not len(line):
            continue
        msg = pynmea2.parse(line)
        p.update(msg)
    except serial.SerialException as e:
        print('Device error: {}'.format(e))
        break
    except pynmea2.ParseError as e:
        print('Parse error: {}'.format(e))
        continue
