from collections import deque
from dataclasses import dataclass, field
from datetime import datetime, timedelta, timezone
from functools import partial
from os import environ
from threading import Thread
from time import monotonic, sleep
from typing import Optional, Tuple

from geopy.distance import geodesic
import gps
import requests


ts_func = partial(datetime.now, tz=timezone.utc)


@dataclass
class Fix:
    pos: Tuple[float, float]
    alt: Optional[float]
    speed: Optional[float]
    track: Optional[float]
    error: Optional[float]
    ts: datetime = field(default_factory=ts_func)

    def __sub__(self: "Fix", other: "Fix"):
        return geodesic(self.pos, other.pos)

    def phonetrack_data(self) -> dict:
        res = {
            "lat": self.lat,
            "lon": self.lon,
            "timestamp": self.ts.timestamp(),
        }
        for key, attr in {
            "speed": "speed",
            "alt": "alt",
            "bearing": "track",
            "acc": "error",
        }.items():
            val = getattr(self, attr, None)
            if val is not None:
                res[key] = val
        return res

    @property
    def lat(self) -> float:
        return self.pos[0]

    @property
    def lon(self) -> float:
        return self.pos[1]


@dataclass
class VertCache:
    alt: float
    error: float
    ts: datetime = field(default_factory=ts_func)


def stream_fixes():
    session = gps.gps(mode=gps.WATCH_ENABLE)
    vert_cache = None
    cache_maxage = timedelta(seconds=1)
    try:
        while session.read() == 0:
            if not (gps.MODE_SET & session.valid) or session.fix.mode < 2:
                continue
            sfix = session.fix
            # print(vars(sfix))
            if gps.isfinite(sfix.altitude) and gps.isfinite(sfix.epv):
                vert_cache = VertCache(alt=sfix.altitude, error=sfix.epv)
            elif vert_cache and vert_cache.ts < ts_func() - cache_maxage:
                vert_cache = None
            if gps.isfinite(sfix.latitude) and gps.isfinite(sfix.longitude):
                error = max(
                    vert_cache.error if vert_cache else 0,
                    sfix.eph if gps.isfinite(sfix.eph) else 0,
                    sfix.epx if gps.isfinite(sfix.epx) else 0,
                    sfix.epy if gps.isfinite(sfix.epy) else 0,
                )
                if error == 0:
                    error = None
                yield Fix(
                    pos=(sfix.latitude, sfix.longitude),
                    alt=vert_cache.alt if vert_cache else None,
                    speed=sfix.speed if gps.isfinite(sfix.speed) else None,
                    track=sfix.track if gps.isfinite(sfix.track) else None,
                    error=error,
                )
    finally:
        session.close()


def filter_fixes(fix_stream, min_delay=1, max_delay=600, min_dist=5):
    prev_fix = None
    prev_time = None
    for fix in fix_stream:
        now = monotonic()
        if prev_fix is None:
            prev_fix = fix
            prev_time = now
            yield fix
            continue
        since_prev = now - prev_time
        if since_prev < min_delay:
            continue
        if (fix - prev_fix).m < min_dist and since_prev < max_delay:
            continue
        prev_fix = fix
        prev_time = now
        yield fix


def retry_sending_fixes(fix_stream, url, timeout=2, bufsize=1000):
    to_send = deque((), bufsize)

    def consumer():
        while True:
            if not to_send:
                sleep(0.5)
                continue
            item = to_send[-1]
            try:
                res = requests.post(
                    url,
                    timeout=timeout,
                    data=item.phonetrack_data(),
                )
                if res.status_code < 500:
                    # This item is either faulty, or was handled successfully.
                    try:
                        to_send.remove(item)
                    except ValueError:
                        # No longer in the deque, maybe got pushed off.
                        pass
                print(res, len(to_send))
            except Exception:
                # Retry.
                pass

    cons_thread = Thread(target=consumer, name="Submitter", daemon=True)
    cons_thread.start()

    for fix in fix_stream:
        to_send.append(fix)
        yield fix


def run(phonetrack_url: str, session_token: str, dev_name: str, min_delay=5):
    stream = retry_sending_fixes(
        filter_fixes(
            stream_fixes(),
            min_delay=min_delay,
        ),
        f"{phonetrack_url}/logPost/{session_token}/{dev_name}",
        timeout=min_delay/2,
    )
    for fix in stream:
        print(fix)


if __name__ == "__main__":
    run(environ["PHONETRACK_URL"], environ["PHONETRACK_SESSION"], environ["PHONETRACK_DEVICE"])
