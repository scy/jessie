from binascii import hexlify
from datetime import datetime, timezone
from os import environ as env
import struct

import paho.mqtt.client as mqtt


PAYLOAD_LEN = 23


def on_connect(client, userdata, flags, rc, props=None):
    client.subscribe(env.get("JESSIE_MQTT_PREFIX") + "gnss/packed")


def on_message(client, userdata, msg):
    try:
        if len(msg.payload) != PAYLOAD_LEN:
            return
        datfile.write(msg.payload)
        ts, lat, lon, alt, spd, crs, qs = struct.unpack("!dffeeeB", msg.payload)
        qual = (qs & 0xf0) >> 4
        sats = qs & 0x0f
        if qual == 15:
            qual = None
        if sats == 15:
            sats = None
        print("{} ({}, {}) {}m @ {} km/h, {}°, q{}, {} sats".format(
            datetime.fromtimestamp(ts, timezone.utc),
            round(lat, 6), round(lon, 6), round(alt, 1),
            round(spd), round(crs),
            "?" if qual is None else qual,
            "?" if sats is None else sats,
        ))
    except:
        pass


mqttc = mqtt.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message

mqttc.connect(env.get("JESSIE_MQTT_HOST"))
mqttc.username_pw_set(env.get("JESSIE_MQTT_USER"), env.get("JESSIE_MQTT_PASS"))

with open("jessie-gnss-log.dat", "ab", buffering=PAYLOAD_LEN) as datfile:
    mqttc.loop_forever()
