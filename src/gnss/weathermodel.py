from datetime import date, datetime, time, timezone
import json
from time import strptime
from typing import List, Optional

from pydantic import BaseModel, validator


def compact_json_dumps(obj, **kwargs):
    return json.dumps(obj, **kwargs, indent=None, separators=(",", ":"))


class ValueStr(BaseModel):
    value: str


class Condition(BaseModel):
    FeelsLikeC: int
    cloudcover: int
    humidity: int
    localObsDateTime: datetime
    observation_time: time
    precipMM: float
    pressure: int
    temp_C: int
    uvIndex: int
    visibility: int
    weatherDesc: List[ValueStr]
    winddirDegree: int
    windspeedKmph: int

    @validator("localObsDateTime", pre=True)
    def prepare_datetime(cls, v):
        t = strptime(v, "%Y-%m-%d %I:%M %p")
        return datetime(t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min)

    @validator("observation_time", pre=True)
    def prepare_time(cls, v):
        t = strptime(v, "%I:%M %p")
        return time(t.tm_hour, t.tm_min, tzinfo=timezone.utc)


class Area(BaseModel):
    areaName: List[ValueStr]
    region: List[ValueStr]
    country: List[ValueStr]
    latitude: float
    longitude: float


class Astronomy(BaseModel):
    moon_illumination: int
    moon_phase: str
    moonrise: Optional[time]
    moonset: Optional[time]
    sunrise: Optional[time]
    sunset: Optional[time]

    @validator("moonrise", "moonset", "sunrise", "sunset", pre=True)
    def prepare_time(cls, v):
        try:
            t = strptime(v, "%I:%M %p")
        except ValueError:
            return None
        return time(t.tm_hour, t.tm_min)


class Hour(BaseModel):
    FeelsLikeC: int
    chanceoffog: int
    chanceoffrost: int
    chanceofrain: int
    chanceofsnow: int
    chanceofthunder: int
    cloudcover: int
    humidity: int
    precipMM: float
    pressure: int
    tempC: int
    time: int
    uvIndex: int
    visibility: int
    weatherDesc: List[ValueStr]
    winddirDegree: int
    windspeedKmph: int


class Day(BaseModel):
    date: date
    mintempC: int
    avgtempC: int
    maxtempC: int
    sunHour: float
    uvIndex: int
    totalSnow_cm: float
    hourly: List[Hour]
    astronomy: List[Astronomy]


class Report(BaseModel):
    current_condition: List[Condition]
    nearest_area: List[Area]
    weather: List[Day]

    class Config:
        json_dumps = compact_json_dumps
