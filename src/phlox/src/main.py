import asyncio
from machine import Pin, Signal

from phlox.heartbeat import beat as heartbeat
from phlox.microtonic import Votonic
from phlox.rs485 import RS485
from phlox.temphum import Scanner as TempHumScanner
from phlox.uptime import export_uptime_metric
from phlox.watchdog import MultiWatchdog


# ruff: noqa: INP001


async def main(*, hb_led: int) -> None:
    watchdog = MultiWatchdog()
    votronic = Votonic(
        RS485(
            uart_id=2,
            baud=19_200,
            bits=8,
            parity=0,
            stop=1,
            re=Signal(Pin(2, Pin.OUT), invert=True),
            te=Signal(Pin(4, Pin.OUT), invert=False),
        )
    )

    temphum = TempHumScanner(
        {
            "4C:65:A8:DC:06:78": "cockpit",
            "4C:65:A8:DC:1B:6A": "desk",
            "4C:65:A8:DC:1C:2C": "bathroom",
            "4C:65:A8:DC:29:07": "upper-storage",
            "4C:65:A8:DC:30:A6": "bed",
            "58:2D:34:3B:24:21": "lower-storage",
            "58:2D:34:3B:24:29": "front-storage",
            "58:2D:34:3B:25:53": "heater",
            "58:2D:34:3B:27:9C": "water-tank",
            "58:2D:34:3B:28:8E": "fridge",
        },
        watchdog=watchdog.register(30),
    )
    tasks = [  # noqa: F841
        asyncio.create_task(heartbeat(hb_led)),
        asyncio.create_task(export_uptime_metric()),
        asyncio.create_task(votronic.watch()),
        asyncio.create_task(votronic.cycle()),
        asyncio.create_task(temphum.run()),
        asyncio.create_task(watchdog.run()),
    ]
    while True:
        await asyncio.sleep_ms(1000)


asyncio.run(
    main(
        hb_led=5,
    )
)
