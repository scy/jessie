from asyncio import sleep_ms
from machine import Pin


PATTERN = (50, 100, 100, 750)


async def beat(pinid: int) -> None:
    pin = Pin(pinid, Pin.OUT)
    while True:
        for idx, delay in enumerate(PATTERN):
            pin.off() if idx % 2 else pin.on()
            await sleep_ms(delay)
