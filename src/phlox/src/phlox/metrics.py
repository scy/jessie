import time


class Gauge:
    def __init__(
        self, name: str, labels: dict[str, str] | None = None
    ) -> None:
        self.name = name
        self.labels = labels or {}
        self.value = None
        self.updated = None

    def update(self, value: float) -> None:
        self.value = value
        self.updated = time.time()
        for line in self.promtext():
            print(line)

    def promlabels(self) -> str:
        return (
            ""
            if len(self.labels) == 0
            else (
                "{"
                + ",".join(f'{k}="{v}"' for k, v in self.labels.items())
                + "}"
            )
        )

    def promtext(self):
        if self.updated is None:
            return
        yield f"# TYPE {self.name} gauge"
        yield f"{self.name}{self.promlabels()} {self.value}"
