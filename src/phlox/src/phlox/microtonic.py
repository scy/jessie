import asyncio
from binascii import hexlify
from struct import unpack
from time import ticks_add, ticks_diff, ticks_ms, time

from .metrics import Gauge
from .packets import align_start


class RawPacket:
    PACKET_LEN = 7

    def __init__(self, data: bytes) -> None:
        if len(data) != self.PACKET_LEN:
            raise ValueError(f"expected {self.PACKET_LEN} bytes")
        self.raw = data
        (
            self.type,
            self.dst,
            self.src,
            self.cmd,
            self.param,
            self.payload,
        ) = unpack("<BBBBB2s", data)

    def __str__(self) -> str:
        direction = {0x22: "-->", 0x62: "<--"}.get(
            self.type, f"x{self.type:02x}"
        )
        if direction == "-->":
            left = self.src
            right = self.dst
            question = "?"
        else:
            left = self.dst
            right = self.src
            question = " "
        cmd = {0x03: "val"}.get(self.cmd, f"x{self.cmd:02x}")
        payload = (
            ""
            if self.payload == b"\x00\x00"
            else hexlify(self.payload).decode()
        )
        return "{:02x} {} {:02x} {} {:02x}{} {}".format(
            left, direction, right, cmd, self.param, question, payload
        )


# Devices
HOUSE_BAT = 0x0C
SOLAR = 0x10
FRESH_W = 0x14
GRAY_W = 0x18
START_BAT = 0x44


class Packet:
    DEVICE = 0x00
    PARAM = 0x00
    POW = 0
    UNIT = ""
    PACKFMT = "H"
    PROM_NAME = "unknown"
    PROM_LABELS = {}
    REQUEST_EVERY = 5

    def __init__(self, raw: RawPacket) -> None:
        self.raw = raw
        (self.value,) = unpack(self.PACKFMT, raw.payload)

    def __str__(self) -> str:
        fmtval = f"{{:.{self.POW}f}}{{}}".format(self.floatval(), self.UNIT)
        return f"{type(self).__name__}: {fmtval}"

    def floatval(self) -> float:
        return self.value / 10**self.POW


class FreshWaterLevel(Packet):
    DEVICE = FRESH_W
    PARAM = 0x02
    UNIT = "%"
    PROM_NAME = "water_level_percent"
    PROM_LABELS = {"tank": "fresh"}
    REQUEST_EVERY = 1800


class GrayWaterLevel(Packet):
    DEVICE = GRAY_W
    PARAM = 0x02
    UNIT = "%"
    PROM_NAME = "water_level_percent"
    PROM_LABELS = {"tank": "gray"}
    REQUEST_EVERY = 1800


class HouseCurrent(Packet):
    DEVICE = HOUSE_BAT
    PARAM = 0x02
    POW = 1
    UNIT = "A"
    PACKFMT = "h"
    PROM_NAME = "current_amperes"
    PROM_LABELS = {"sensor": "house-bat"}
    REQUEST_EVERY = 2


class HouseVoltage(Packet):
    DEVICE = HOUSE_BAT
    PARAM = 0x03
    POW = 2
    UNIT = "V"
    PROM_NAME = "battery_volts"
    PROM_LABELS = {"sensor": "house-bat"}
    REQUEST_EVERY = 10


class HouseCharge(Packet):
    DEVICE = HOUSE_BAT
    PARAM = 0x05
    UNIT = "Ah"
    PROM_NAME = "battery_charge_amperehours"
    PROM_LABELS = {"sensor": "house-bat"}
    REQUEST_EVERY = 10


class HouseChargePercent(Packet):
    DEVICE = HOUSE_BAT
    PARAM = 0x06
    UNIT = "%"
    PACKFMT = "B"
    PROM_NAME = "battery_charge_percent"
    PROM_LABELS = {"sensor": "house-bat"}
    REQUEST_EVERY = 10


class SolarCurrent(Packet):
    DEVICE = SOLAR
    PARAM = 0x02
    POW = 1
    UNIT = "A"
    PROM_NAME = "current_amperes"
    PROM_LABELS = {"sensor": "solar"}
    REQUEST_EVERY = 2


class StartVoltage(Packet):
    DEVICE = START_BAT
    PARAM = 0x03
    POW = 2
    UNIT = "V"
    PROM_NAME = "battery_volts"
    PROM_LABELS = {"sensor": "start-bat"}
    REQUEST_EVERY = 10


class Last:
    def __init__(self, cls: type[Packet]):
        self.cls = cls
        self.gauge = Gauge(cls.PROM_NAME, cls.PROM_LABELS)
        self.packet: Packet | None = None
        self.seen: int | None = None

    def due_at(self) -> int:
        return (self.seen + self.cls.REQUEST_EVERY) if self.seen else 0

    def due_in(self, now: int) -> int:
        return self.due_at() - now

    def update(self, packet: Packet):
        self.seen = time()
        self.packet = packet
        self.gauge.update(packet.floatval())


# Matching Packet class can be looked up by device & param.
KNOWN_PACKET_TYPES = {
    (cls.DEVICE, cls.PARAM): cls
    for cls in (
        FreshWaterLevel,
        GrayWaterLevel,
        HouseCharge,
        HouseChargePercent,
        HouseCurrent,
        HouseVoltage,
        SolarCurrent,
        StartVoltage,
    )
}


class Votonic:
    CHECKSUM_START = 0x55
    PACKET_SIZE = 9
    START = b"\xaa"

    def __init__(self, iface) -> None:  # noqa: ANN001
        self._iface = iface
        self._rxbuf = bytearray(self.PACKET_SIZE)
        self._rxpos = 0
        # For every packet class, create gauge & keep last package.
        self.last = {cls: Last(cls) for cls in KNOWN_PACKET_TYPES.values()}
        # Events to trigger when a requested packet arrives.
        self.waiting: dict[type[Packet], asyncio.Event] = {}

    @classmethod
    def checksum(cls, buffer: bytearray) -> int:
        checksum = cls.CHECKSUM_START
        for byte in buffer:
            checksum ^= byte
        return checksum

    def promtext(self):
        for last in self.last.values():
            yield from last.gauge.promtext()

    async def read(self) -> Packet | RawPacket:
        while True:
            await align_start(self.START, buf=self._rxbuf, reader=self._iface)
            if self.checksum(self._rxbuf) == 0:
                packet = RawPacket(self._rxbuf[1:-1])
                if cls := KNOWN_PACKET_TYPES.get(
                    (packet.src, packet.param), None
                ):
                    return cls(packet)
                return packet

    async def request(self, cls: type[Packet]) -> asyncio.Event:
        buf = self.START + bytes(
            (0x22, cls.DEVICE, 0xF4, 0x03, cls.PARAM, 0x00, 0x00)
        )
        buf += bytes((self.checksum(buf),))
        if cls in self.waiting:
            event = self.waiting[cls]
        else:
            event = self.waiting[cls] = asyncio.Event()
        await self._iface.write(buf)
        return event

    async def query(
        self,
        cls: type[Packet],
        *,
        retry_every: int = 1_123,
        timeout: int = 5_000,
    ) -> Packet | None:
        deadline = ticks_add(ticks_ms(), timeout)
        event = await self.request(cls)

        async def waiter():
            try:
                await event.wait()
            except asyncio.CancelledError:
                pass

        while ticks_diff(deadline, ticks_ms()) > 0:
            if event.is_set():
                return self.last[cls].packet
            try:
                await asyncio.wait_for_ms(waiter(), retry_every)
            except asyncio.TimeoutError:
                event = await self.request(cls)
        return None

    async def watch(self) -> None:
        while True:
            packet = await self.read()
            if isinstance(packet, Packet):
                cls = type(packet)
                self.last[cls].update(packet)
                # Notify requesters that the packet has arrived.
                if cls in self.waiting:
                    self.waiting[cls].set()
                    del self.waiting[cls]

    async def cycle(self) -> None:
        while True:
            await asyncio.sleep_ms(523)
            now = time()
            to_request = min(
                self.last.values(), key=lambda last: last.due_in(now)
            )
            if to_request.due_in(now) > 0:
                continue
            await self.request(to_request.cls)

    async def dump(self) -> None:
        """Read forever and print the packets received."""
        while True:
            packet = await self.read()
            if isinstance(packet, Packet):
                data = packet.raw.raw
                basic = str(packet.raw)
                d = str(packet)
            else:
                data = packet.raw
                basic = str(packet)
                d = ""
            hex_str = hexlify(data, " ").decode()
            print(f"{ticks_ms()}|  {hex_str}  {basic:22}  {d}")  # noqa: T201
