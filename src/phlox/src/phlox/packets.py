async def align_start(start: bytes, *, buf: bytearray, reader) -> None:  # noqa: ANN001
    if len(start) != 1:
        raise NotImplementedError("only single start bytes supported")
    pos = offset = chunklen = 0
    view = memoryview(buf)
    viewlen = len(view)
    while True:
        # Read into the buffer until it's full.
        while pos < viewlen:
            pos += await reader.readinto(view[pos:])
        # Look for the start byte.
        offset = buf.find(start)
        if offset == -1:
            # Not found at all, start over.
            pos = 0
        elif offset == 0:
            # Found at the start, return successfully.
            return
        else:
            # Found somewhere in the middle. Shift to the front and continue
            # filling up the buffer.
            chunklen = viewlen - offset
            view[0:chunklen] = view[offset:]
            pos = chunklen
