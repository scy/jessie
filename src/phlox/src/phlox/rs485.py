import asyncio
import machine
from binascii import hexlify
from math import ceil
from time import ticks_diff, ticks_us


class RS485:
    def __init__(  # noqa: PLR0913
        self,
        *,
        uart_id: int,
        re: machine.Signal | None = None,
        te: machine.Signal | None = None,
        baud: int = 9_600,
        bits: int = 8,
        parity: int | None = None,
        stop: int = 1,
    ):
        self._uart = machine.UART(uart_id, baud, bits, parity, stop)
        bits_per_byte = 1 + bits + (0 if parity is None else 1) + stop
        self._usec_per_byte = 1_000_000 * bits_per_byte / baud

        self._re = re
        if self._re:
            self._re.on()

        self._te = te
        if self._te:
            self._te.off()

        self.reader = asyncio.StreamReader(self._uart)
        self.writer = asyncio.StreamWriter(self._uart)

        self.readinto = self.reader.readinto

    async def dump(self, bufsize: int = 32) -> None:
        """Read forever from RS485 and print the data received."""
        while True:
            data = await self.reader.read(bufsize)
            print("RS485: " + hexlify(data, " ").decode())  # noqa: T201

    async def write(self, buffer: bytes) -> int:
        try:
            if self._te:
                self._te.on()
                # The Arduino library sleeps 50 µs by default.
                await asyncio.sleep_ms(1)
            self.writer.write(buffer)
            start_drain = ticks_us()
            await self.writer.drain()
            # Wait for as long as it should take to send these bytes, minus
            # what we've already been waiting in drain().
            await asyncio.sleep_ms(
                ceil(
                    (
                        len(buffer) * self._usec_per_byte
                        - ticks_diff(ticks_us(), start_drain)
                    )
                    / 1000
                )
            )
            end_drain = ticks_us()
        finally:
            if self._te:
                self._te.off()
        return ticks_diff(end_drain, start_drain)
