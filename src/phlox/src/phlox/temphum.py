from binascii import unhexlify

import aioble
from xiaomi_ble_adv_parse import parse_adv_data

from .metrics import Gauge


def bytesaddr(addr: str | bytes) -> bytes:
    if isinstance(addr, bytes):
        return addr
    if isinstance(addr, str):
        return unhexlify(
            "".join(filter(lambda c: c in "0123456789abcdef", addr.lower()))
        )
    raise TypeError("expected str or bytes")


class Device:
    def __init__(self, *, addr: bytes, name: str) -> None:
        self.addr = addr
        self.name = name
        labels = {"sensor": name}
        self.bat = Gauge("battery_charge_percent", labels)
        self.hum = Gauge("env_rel_humidity_percent", labels)
        self.temp = Gauge("env_temperature_celsius", labels)

    def handle_adv(self, adv_data: bytes) -> None:
        if parsed := parse_adv_data(adv_data):
            if "t" in parsed:
                self.temp.update(parsed["t"])
            if "h" in parsed:
                self.hum.update(parsed["h"])
            if "b" in parsed:
                self.bat.update(parsed["b"])

    def promtext(self):
        for gauge in (self.temp, self.hum, self.bat):
            yield from gauge.promtext()


class Scanner:
    def __init__(self, devices: dict[str | bytes, str], watchdog=None) -> None:
        self.devices = {
            baddr: Device(addr=baddr, name=name)
            for addr, name in devices.items()
            if (baddr := bytesaddr(addr))
        }
        self.watchdog = watchdog

    def promtext(self):
        for device in self.devices.values():
            yield from device.promtext()

    async def run(self) -> None:
        async with aioble.scan(0, 55_000, 25_250, active=False) as scanner:
            async for result in scanner:
                if device := self.devices.get(result.device.addr, None):
                    device.handle_adv(result.adv_data)
                    if self.watchdog:
                        self.watchdog.update()
