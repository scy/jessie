import asyncio
import time

from .metrics import Gauge


async def export_uptime_metric(interval: int = 5):
    started = time.time()
    uptime = Gauge("node_uptime_seconds")
    while True:
        uptime.update(time.time() - started)
        await asyncio.sleep(interval)
