import asyncio
from machine import WDT
import os
import time


class Watcher:
    def __init__(self, watchdog: "MultiWatchdog") -> None:
        self.updated = time.time()
        self.watchdog = watchdog

    def update(self) -> None:
        self.updated = time.time()
        self.watchdog.update()


class MultiWatchdog:
    def __init__(self) -> None:
        self.watchers: list[tuple[Watcher, int]] = []
        self.wdt: WDT | None = None

    def register(self, timeout_s: int) -> Watcher:
        watcher = Watcher(self)
        self.watchers.append((watcher, timeout_s))
        return watcher

    def alive(self) -> bool:
        now = time.time()
        for watcher, timeout_s in self.watchers:
            if watcher.updated < now - timeout_s:
                return False
        return True

    def disabled(self) -> bool:
        try:
            os.stat("/.disable-watchdog")
            return True
        except OSError:
            pass
        return False

    async def run(self, timeout_ms: int = 5_000):
        if self.disabled():
            return
        self.wdt = WDT(timeout=timeout_ms)
        sleep_interval = timeout_ms // 2
        while True:
            await asyncio.sleep_ms(sleep_interval)
            self.update()

    def update(self) -> None:
        if self.wdt and self.alive():
            self.wdt.feed()
